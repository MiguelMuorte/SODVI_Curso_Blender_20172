﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PlayerController : MonoBehaviour {

    public Animator anim;
    public NavMeshAgent nav;
    public LayerMask layerMask;

    Camera cam;
    Ray ray;
    RaycastHit hitInfo;

	void Start () {
        this.gameObject.tag = "Player";
        cam = Camera.main;
    }
	
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, 15f, layerMask) && hitInfo.collider.tag == "Floor")
            {
                //StopAllCoroutines();
                StartCoroutine(SetNewPath(hitInfo.point));
            }
            Debug.DrawRay(ray.origin, ray.direction * 15f, Color.red, 3f);
        }
        anim.SetFloat("Blend", nav.velocity.magnitude);
        
    }

    IEnumerator SetNewPath(Vector3 dest)
    {
        nav.isStopped = true;
        nav.destination = dest;
        while (nav.pathPending) yield return null;
        nav.isStopped = false;
    }

    void OpenChest()
    {
        nav.isStopped = true;
        anim.SetTrigger("Open");
    }
}
